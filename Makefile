# Ce make file permet de generer le site
# make init
# => installe les dependances python
# make 
# => génère le site
# make serve
# => lance une preview du site sur le port 8000
DEFAULT_GOAL := all

MKDOCS := ${HOME}/.local/bin/mkdocs

all:
	$(MKDOCS) build
	
init:
	pip3 install mkdocs mkdocs-material --user

serve: 
	$(MKDOCS) serve

