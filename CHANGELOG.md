# CHANGELOG

## Delorean v1.1.0

- réacteur -> consomme des détritus
- climatisation
- roue -> mode hiver
- lentille gravitationnelle
- parebrise -> filtre anti UVB
- Correction bug parebrise
- modification transparence parebrise

## Delorean v1.0.0

- des roues
- un pare brise
- un convecteur temporel
- un selecteur temporel
- un reacteur

